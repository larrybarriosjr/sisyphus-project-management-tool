import React, { Component } from "react"
import { bindActionCreators } from "redux"
import { connect } from "react-redux"
import * as actions from "../redux/modules/project"
import { Container, Grid, Box, TextField } from "@material-ui/core"
import PageHeading from "../components/PageHeading"
import CreateButton from "../components/CreateButton"
import ProjectTable from "../components/ProjectTable"
import CreateModal from "../components/CreateModal"
import map from "lodash/map"

const columns = [
  { key: 1, name: "Name" },
  { key: 2, name: "Key" },
  { key: 3, name: "Type" },
  { key: 4, name: "Lead" },
  { key: 5, name: "Category" },
  { key: 6, name: "URL" },
  { key: 7, name: "Starred" }
]

const initialValues = {
  project_name: "",
  project_key: "",
  project_type: "",
  project_lead: "",
  project_category: "",
  project_url: "",
  project_starred: false
}

const fields = [
  { key: 1, label: "Name", name: "project_name" },
  { key: 2, label: "Key", name: "project_key" },
  { key: 3, label: "Type", name: "project_type" },
  { key: 4, label: "Lead", name: "project_lead" },
  { key: 5, label: "Category", name: "project_category" },
  { key: 6, label: "URL", name: "project_url" },
  { key: 7, label: "Starred", name: "project_starred" }
]

class Project extends Component {
  constructor(props) {
    super(props)
    this.handleOpen = this.handleOpen.bind(this)
    this.handleClose = this.handleClose.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
  }

  handleOpen() {
    this.props.toggleCreateModal(true)
  }
  handleClose() {
    this.props.toggleCreateModal(false)
  }
  handleSubmit(project) {
    this.props.createProject(project)
    this.props.toggleCreateModal(false)
  }

  render() {
    return (
      <Container>
        <Box m="2.5rem">
          <Grid container justify="space-between">
            <PageHeading>Projects</PageHeading>
            <CreateButton onClick={this.handleOpen}>
              Create Project
            </CreateButton>
            <CreateModal
              title="Create project"
              open={this.props.createModal}
              onClose={this.handleClose}
              onSubmit={this.handleSubmit}
              initialValues={initialValues}
            >
              {props =>
                map(fields, input => (
                  <Box my="0.5rem">
                    <TextField
                      label={input.label}
                      name={input.name}
                      onChange={props.handleChange}
                      onBlur={props.handleBlur}
                      value={props.values[input.name]}
                      variant="outlined"
                    />
                  </Box>
                ))
              }
            </CreateModal>
          </Grid>
        </Box>
        <Box mx="2.5rem">
          <ProjectTable
            columns={columns}
            data={this.props.projects}
            slug={this.props.history}
          />
        </Box>
      </Container>
    )
  }
}

function mapStateToProps(state) {
  return { ...state.project }
}
function mapDispatchToProps(dispatch) {
  return bindActionCreators({ ...actions }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(Project)

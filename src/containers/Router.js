import React, { Component } from "react"
import { BrowserRouter, Route, Switch } from "react-router-dom"
import map from "lodash/map"
import App from "../app/App"
import Project from "./Project"

const routes = [
  { key: 1, path: "/", component: App, exact: true },
  { key: 2, path: "/projects", component: Project, exact: true }
]

class Router extends Component {
  constructor(props) {
    super(props)
    this.handleRoutes = this.handleRoutes.bind(this)
  }

  renderRoutes = this.handleRoutes()

  handleRoutes() {
    return map(routes, ({ ...route }) => <Route {...route} />)
  }

  render() {
    return (
      <BrowserRouter>
        <Switch>{this.renderRoutes}</Switch>
      </BrowserRouter>
    )
  }
}

export default Router

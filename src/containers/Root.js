import React, { Component } from "react"
import { Provider } from "react-redux"
import Theme from "./Theme"
import configureStore from "../redux/configureStore"

const store = configureStore()

class Root extends Component {
  render() {
    return (
      <Provider store={store}>
        <Theme />
      </Provider>
    )
  }
}

export default Root

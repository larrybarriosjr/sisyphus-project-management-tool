import React, { Component } from "react"
import { createMuiTheme, ThemeProvider } from "@material-ui/core/styles"
import Router from "./Router"
import { CssBaseline } from "@material-ui/core"

const dark = createMuiTheme({
  palette: {
    type: "dark"
  }
})

class Theme extends Component {
  render() {
    return (
      <ThemeProvider theme={dark}>
        <CssBaseline />
        <Router />
      </ThemeProvider>
    )
  }
}

export default Theme

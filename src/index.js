import React from "react"
import { render } from "react-dom"
import { unregister } from "./serviceWorker"
import Root from "./containers/Root"
import "./index.css"

const RootHTML = document.getElementById("root")
render(<Root />, RootHTML)
unregister()

import React, { Component, Fragment } from "react"
import {
  Dialog,
  DialogTitle,
  DialogContent,
  DialogActions,
  Button,
  Grid
} from "@material-ui/core"
import { Formik } from "formik"

class CreateModal extends Component {
  render() {
    return (
      <Dialog open={this.props.open} onClose={this.props.onClose} scroll="body">
        <DialogTitle>{this.props.title}</DialogTitle>
        <DialogContent>
          <Formik
            initialValues={this.props.initialValues}
            onSubmit={this.props.onSubmit}
          >
            {props => (
              <Fragment>
                <Grid container direction="column">
                  {this.props.children(props)}
                </Grid>
                <DialogActions>
                  <Button onClick={this.props.onClose}>Cancel</Button>
                  <Button onClick={props.handleSubmit}>Create</Button>
                </DialogActions>
              </Fragment>
            )}
          </Formik>
        </DialogContent>
      </Dialog>
    )
  }
}

export default CreateModal

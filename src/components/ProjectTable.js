import React, { Component } from "react"
import map from "lodash/map"
import {
  Table,
  TableContainer,
  TableHead,
  TableRow,
  TableCell,
  TableBody,
  TableSortLabel,
  TableFooter,
  Paper,
  TablePagination
} from "@material-ui/core"

class ProjectTable extends Component {
  constructor(props) {
    super(props)
    this.state = {
      page: 0,
      rowsPerPage: 5
    }
    this.computeDataPerPage = this.computeDataPerPage.bind(this)
    this.handleRowSelect = this.handleRowSelect.bind(this)
    this.handleChangePage = this.handleChangePage.bind(this)
    this.handleChangeRowsPerPage = this.handleChangeRowsPerPage.bind(this)
  }

  computeDataPerPage() {
    const rowStart = this.state.page * this.state.rowsPerPage
    const rowEnd = rowStart + this.state.rowsPerPage
    if (this.state.rowsPerPage > 0) {
      return this.props.data.slice(rowStart, rowEnd)
    } else {
      return this.props.data
    }
  }

  handleRowSelect(slug) {
    return function(row) {
      return function() {
        return slug.push("/project/" + row.name)
      }
    }
  }

  handleChangePage(e, page) {
    this.setState({ page })
  }
  handleChangeRowsPerPage(e) {
    this.setState({ page: 0, rowsPerPage: e.target.value })
  }

  render() {
    return (
      <TableContainer component={Paper}>
        <Table>
          <TableHead>
            <TableRow>
              {map(this.props.columns, column => (
                <TableCell key={column.key}>
                  <TableSortLabel />
                  {column.name}
                </TableCell>
              ))}
            </TableRow>
          </TableHead>
          <TableBody>
            {map(this.computeDataPerPage(), row => (
              <TableRow
                key={row.name}
                hover
                onClick={this.handleRowSelect(this.props.slug)(row)}
              >
                {map(row, (item, i) => (
                  <TableCell key={item + i}>{item}</TableCell>
                ))}
              </TableRow>
            ))}
          </TableBody>
          <TableFooter>
            <TableRow>
              <TablePagination
                count={this.props.data.length}
                page={this.state.page}
                rowsPerPage={this.state.rowsPerPage}
                rowsPerPageOptions={[5, 10, 50, 100]}
                onChangePage={this.handleChangePage}
                onChangeRowsPerPage={this.handleChangeRowsPerPage}
              />
            </TableRow>
          </TableFooter>
        </Table>
      </TableContainer>
    )
  }
}

export default ProjectTable

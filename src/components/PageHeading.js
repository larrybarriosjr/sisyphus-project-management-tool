import React, { Component } from "react"
import Typography from "@material-ui/core/Typography"

class PageHeading extends Component {
  render() {
    return (
      <Typography variant="h4" component="h2">
        {this.props.children}
      </Typography>
    )
  }
}

export default PageHeading

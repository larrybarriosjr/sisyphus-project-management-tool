import React, { Component } from "react"
import { Button } from "@material-ui/core"

class CreateButton extends Component {
  render() {
    return (
      <Button color="primary" variant="contained" onClick={this.props.onClick}>
        {this.props.children}
      </Button>
    )
  }
}

export default CreateButton

import { createStore } from "redux"
import rootReducer from "./modules/reducer"

function configureStore(preloadedState) {
  return createStore(rootReducer, preloadedState)
}

export default configureStore

// CONSTANTS
const CREATE_PROJECT = "CREATE_PROJECT"
const TOGGLE_CREATE_MODAL = "TOGGLE_CREATE_MODAL"

// ACTIONS
export function createProject(project) {
  return { type: CREATE_PROJECT, project }
}
export function toggleCreateModal(value) {
  return { type: TOGGLE_CREATE_MODAL, value }
}

// STATES
const initialState = {
  createModal: false,
  projects: [
    createData("BitBucket", "BB", "Software", "Zachary Davis", "", "", false),
    createData("3lo Test", "HVHF", "Software", "Athan Zikou", "", "", false),
    createData("3sia", "CZAM", "Software", "Imran Parvez", "", "", false),
    createData("BitBuckets", "BB", "Software", "Zachary Davis", "", "", false),
    createData("3lo Tests", "HVHF", "Software", "Athan Zikou", "", "", false),
    createData("3sias", "CZAM", "Software", "Imran Parvez", "", "", false),
    createData("BitBuscket", "BB", "Software", "Zachary Davis", "", "", false),
    createData("3lo Tesst", "HVHF", "Software", "Athan Zikou", "", "", false),
    createData("3sisa", "CZAM", "Software", "Imran Parvez", "", "", false),
    createData("BsitBucket", "BB", "Software", "Zachary Davis", "", "", false),
    createData("3slo Test", "HVHF", "Software", "Athan Zikou", "", "", false),
    createData("3ssia", "CZAM", "Software", "Imran Parvez", "", "", false)
  ]
}

// REDUCER
export default function(state = initialState, action) {
  switch (action.type) {
    case TOGGLE_CREATE_MODAL:
      return { ...state, createModal: action.value }
    case CREATE_PROJECT:
      return {
        ...state,
        projects: [...state.projects, action.project]
      }
    default:
      return state
  }
}

function createData(name, key, type, lead, category, url, starred) {
  return { name, key, type, lead, category, url, starred }
}
